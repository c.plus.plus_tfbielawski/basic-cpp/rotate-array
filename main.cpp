/* CPP Interview Questions Bootcamp
// Rotate Array
// @AlexKropova
// 12/22/2020
*/

/* Rotate an array of X elements to the left by Y number of steps */

#include <iostream>
using namespace std;

/* Functions */

// Function to find number of sets
int findNumberOfSets(int number, int elements)
{
    //Check for "edge case"-- if the array is empty
    if (elements == 0)
    {
        return number;
    }

    //Otherwise proceed here
    else
    {
        //Recursively call this function
        //number becomes elements
        //Modulus number by elements
        //Process repeats until the number of sets is found
        return findNumberOfSets(elements, number % elements);

    }

}

// Function to perform left rotation with helper function from above
void leftRotation (int inputArray[], int number, int elements )
{
    //If num of sets is too large, don't perform or do some math to get proper result
    //This provides the remainder of sets needed to take if multiple rotations
    number = number % elements;

    //Call the find number of sets function, assign to setsToRotate
    int setsToRotate = findNumberOfSets(number, elements);

    //Loop through each element in number of sets, and then move array
    //set index to  0, while index is less than number, increment index
    for (int index = 0; index < setsToRotate; index++)
    {
        //Crete a temporary int, assign the value of the input array
        //at its current value which is [index]
        int temporaryElement = inputArray[index];

        //Declare an integer assign index to it
        int savedIndex = index;

        //Loop through all indices and perform rotation
        //An infinite loop, it uses a break
        while (1)
        {
            //Add saved index + number, assign to temporaryIndex
            int temporaryIndex = savedIndex + number;

            //If temp index is >= elements
            if (temporaryIndex >= elements)
            {
                //Deduct elements from temp index and assign to temp index
                temporaryIndex = temporaryIndex - elements;

                // If the temp index = index, break out of the rotation
                if (temporaryIndex == index) { break; }
            }

            //Assign the [temp index] to [saved index]
            inputArray[savedIndex] = inputArray[temporaryIndex];
            savedIndex = temporaryIndex;

        }
        //Assign the temporary element to the inputArray's saved index
        inputArray[savedIndex] = temporaryElement;
    }


}

// Function to print the array
void printArray(int input[], int size)
{
    //Initialize element to 0, while element is less than size, increment element
    for (int element = 0; element < size ; element++)
    {
        //Display the value of element
        cout << input[element] ;
    }
}

// Main
int main()
{
    //Declare and initialize int array
    int input[] = {1, 2, 3, 4, 5, 6, 7 };

    //
    int size = sizeof(input) / sizeof(input[0]);

    //Call the left rotate array function
    leftRotation(input, 2, size);

    //Call the print function
    printArray(input, size);

    return 0;
}
